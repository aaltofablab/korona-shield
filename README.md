# Korona Shield

> **WARNING!** Not tested yet!

A protective shield design for Finland based on the [OpenFacePPE](https://open-face-website.now.sh/instructions) design by [New York University](http://www.nyu.edu/about/news-publications/news/2020/march/NYUResponds.html). 


## Introduction

These shields are easy to make and do not require 3D printing. The materials used are easy to sterilize if needed. This repository holds data speciffic to shield production in Finland. Units are in millimeters. Read through the BOM and contribute if you see better options for supply chains.

If you're assembling the shields, please download the instructions below and [watch the video](https://youtu.be/haZ7mYbM1eA) to learn how to properly prepare the shields for delivery.


### Design Files

|Name |Link |
|Face Shield Outline DXF |[Download](design-files/Flat Pack Face Shield.dxf) |
|Head Band DXF |[Download](design-files/Flat Pack Forhead Strap .01inch thickness.dxf) |


### Instructions 

|Name |Link |
|Fabrication Guide |[Download](instructions/face-shield-fabrication-flatpack.pdf) |
|Assembly Instructions |[Download](instructions/face-shield-assembly-flatpack.pdf) |


## Bill of Materials (BOM)

|Component |Description |Size |
|--- |--- |--- |
|Clear Plastic Face Shield |0.2 - 0.3 mm thick non-brittle clear material. PET or polycarbonate is good. |318 x 261 mm |
|Forehead Strap |Same material as the Face Shield |26 x 273 mm |
|Elastic |7 - 15 mm wide elastic band |406 mm |
|Foam (optional) |25x13 mm strip |254 mm |


## Supply Sources

The most important material is the transparent plastic sheet. PET sheet is the material of choice by NYU, but almost any clear flexible material should work (such as polycarbonate, PETA, PETG, vinyl, etc). 


### [Foiltek](https://foiltek.fi/)

Source for clear plastic material. They provide a PDF catalogue with materials they can have in stock. Below is a range of products that could be used for the shields.

> **WARNING!** - adding foam makes the shield much more difficult to clean.

|Name |Code |Size |Price |
|--- |--- |--- |--- |
|POLYCARBONATE PC |10540002062 |0,75 x 1250 x 2050 mm |15,00 €/m2 |
|POLYESTER A-PET |10540003323 |0,8 x 1250 x 2050 mm |11.00 €/m2 |
|POLYESTER PET-G |10540002084 |0,5 x 1250 x 2050 mm |10.00 €/m2 |

### [Vink](https://www.vink.fi/)

Source for plastic sheets.

|Name |Code |Size |Price |
|--- |--- |--- |--- |
|LUMEX A APET CLEAR |244472 |1 x 1250 x 2050 mm |10.00 €/m2 |

### [Eurokangas](https://www.eurokangas.fi/)

Elastic band supply. 

|Name and link |Size |Price |
|--- |--- |--- |
|[Elastic band 15 mm black](https://www.eurokangas.fi/kuminauha-pehmea-15mm-50m-m100-5511043m100) |50 m |1.00 €/m |
|[Elastic band 15 mm white](https://www.eurokangas.fi/kuminauha-pehmea-15mm-50m-a100-5511043a100) |50 m |1.00 €/m |
|[Elastic band 7 mm white](https://www.eurokangas.fi/kuminauha-7mm-10m-a100-5516270a100) |10 m |3.00 €/pack |


## Sterilizing

To be continued...


## Licence

This design is released under the [Creative Commons CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/).


## Warranty

> This design is provided with NO WARRANTIES. Prototypes have been used in a hospital setting with positive feedback. If you’re interested or capable of conducting further testing in hospital settings, please reach out via covid19.taskforce@nyu.edu

 


